import os
import httpclient
import json
import strutils

const GEO_URL:string = "http://ip-api.com/json"
const WEATHER_URL:string = "http://api.openweathermap.org/data/2.5/weather?lat=$1&lon=$2&APPID=$3&units=metric"
var client = newHttpClient()

type
  LonLat = tuple[lat:float, lon:float, city:string]

type
  Weather = tuple[main: string, description:string, id:BiggestInt, temperature:BiggestInt, pressure:BiggestInt]

proc getApiKey(): string =
  var api_key:TaintedString = getEnv("WEATHER_API_KEY")
  if api_key.string == "":
    raise new(ValueError)
  return api_key.string

proc getLonLat(): LonLat =
  var response:string = client.getContent(GEO_URL)
  let jsonNode = parseJson(response)
  let ll:LonLat = (lat: jsonNode["lat"].getFNum, lon: jsonNode["lon"].getFNum, city:  jsonNode["city"].getStr)
  return ll

proc idToUnicode(id: BiggestInt):string =
  #ugh
  var res:string = ""
  if id >= 200 and id < 300:
    res = "☈"
  if id >= 300 and id < 400:
    res = "🌦"
  if id >= 500 and id < 600:
    res = "🌦"
  if id >= 600 and id < 700:
    res = "❄"
  if id == 800:
    res = "☼"
  return res

proc formatOutput(lonlat: LonLat, weather:Weather):string =
  return format("$1:$2 $3°C", lonlat.city, idToUnicode(weather.id), weather.temperature)

proc getWeather(lonlat: LonLat, api_key:string):Weather =
  let call_api: string = format(WEATHER_URL, lonlat.lat, lonlat.lon, api_key)
  let response:string = client.getContent(call_api)
  let weather = parseJson(response)["weather"][0]
  let main = parseJson(response)["main"]
  let res:Weather = (
    main: weather["main"].getStr, 
    description: weather["description"].getStr,
    id: weather["id"].getNum,
    temperature: main["temp"].getNum,
    pressure: main["pressure"].getNum
  )
  return res

proc main() =
  var api_key:string
  var lonlat:LonLat = getLonLat()

  try:
    api_key = getApiKey()
  except ValueError:
    echo "No proper WEATHER_API_KEY"
    quit(0)

  let weather:Weather = getWeather(lonlat, api_key)
  echo formatOutput(lonlat, weather)

main()
