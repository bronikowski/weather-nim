# Package

version       = "0.1.0"
author        = "Emil Oppeln-Bronikowski"
description   = "Show weather on terminal"
license       = "MIT"
srcDir        = "src"
bin           = @["weather_nim"]

# Dependencies

requires "nim >= 0.17.2"
